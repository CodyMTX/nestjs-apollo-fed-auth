import { Injectable, NestMiddleware, UnauthorizedException } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: NextFunction) {
    // console.log('Request...');
    // let payload = "";
    // if (req.headers.authorization == '12345') {
        // console.log(req)
        // throw new UnauthorizedException;  
    // }
    console.log("in middleware")
    next();
  }
}