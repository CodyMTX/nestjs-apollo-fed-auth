import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';


@Injectable()
export class FirebaseStrategy extends PassportStrategy(Strategy) {
  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: true,
      secretOrKey: process.env.FIREBASE_JWT_SECRET,
      audience: process.env.FIREBASE_JWT_AUDIENCE,
      issuer: process.env.FIREBASE_JWT_ISSUER,
      passReqToCallback: true
    });
  }

  async validate(request: Request, payload: any) {
    return { userId: payload.sub, username: payload.username };
  }
}