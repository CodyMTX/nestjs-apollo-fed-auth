import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { GATEWAY_BUILD_SERVICE, GraphQLGatewayModule } from '@nestjs/graphql';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { RemoteGraphQLDataSource } from '@apollo/gateway';
import { ConfigModule } from '@nestjs/config';
// import { jwtConstants } from './auth/constants';
import { verify } from 'jsonwebtoken';
import { LoggerMiddleware } from './auth/middleware/logger.middleware';

class AuthenticatedDataSource extends RemoteGraphQLDataSource {
  async willSendRequest({ request, context }) {
    if (context.jwt) {
      const token = context.jwt.replace('Bearer ', '')
      await verify(token, process.env.JWT_SECRET, {
        complete: true,
        ignoreExpiration: true
      }, function(err, decoded) {
        if (!err) {
          request.http.headers.set('x-user-id', decoded.payload.sub);
          request.http.headers.set('x-user-role', decoded.payload.roles);
          // console.log(request.http.headers)
        }
        return err
      });
    }
  }
}

/** The BuildServiceModule is used for sharing context in Apollo Federation
 *  https://docs.nestjs.com/graphql/federation#sharing-context
 */
@Module({
  providers: [
    {
      provide: AuthenticatedDataSource,
      useValue: AuthenticatedDataSource,
    },
    {
      provide: GATEWAY_BUILD_SERVICE,
      useFactory: (AuthenticatedDataSource) => {
        return ({ name, url }) => new AuthenticatedDataSource({ url });
      },
      inject: [AuthenticatedDataSource],
    },
  ],
  exports: [GATEWAY_BUILD_SERVICE],
})
class BuildServiceModule {}

@Module({
  imports: [
    ConfigModule.forRoot(), // for .env variables
    GraphQLGatewayModule.forRootAsync({
      useFactory: async () => ({
        server: {
          cors: true,
          context: ({ req }) => ({
            jwt: req.headers.authorization,
            test:"from GraphQLModule" 
          }),
        },
        gateway: {
          serviceList: [
            { name: 'users', url: 'http://localhost:3000/graphql' },
            { name: 'posts', url: 'http://localhost:3001/graphql' },
          ],
        },
      }),
      imports: [BuildServiceModule],
      inject: [GATEWAY_BUILD_SERVICE],
    }),
    AuthModule,
    UsersModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule  {}
