import { Module } from '@nestjs/common';
import { GraphQLFederationModule, GraphQLSchemaBuilderModule, GraphQLSchemaFactory } from '@nestjs/graphql';
import { GraphQLSchema, printSchema } from 'graphql';
import { applyMiddleware } from 'graphql-middleware';
import { rule, shield } from 'graphql-shield';
import { UsersResolver } from './users.resolver';
import { UsersService } from './users.service';

const isAuthenticated = rule({ cache: 'contextual' })(async (parent, args, ctx, info) => {
  return ctx.user !== null
})

const isAdmin = rule({ cache: 'contextual' })(async (parent, args, ctx, info) => {
  // return ctx.user.role === 'admin'
  // console.log(ctx)
  return ctx.req.headers["x-user-role"] === 'ADMIN'
})

const isEditor = rule({ cache: 'contextual' })(async (parent, args, ctx, info) => {
  console.log(ctx.user.role)
  return ctx.user.role === 'editor'
})

const permissions = shield({
  Query: {
    whoAmI: isAdmin
  }
});
@Module({
  providers: [UsersResolver, UsersService],
  imports: [
    GraphQLFederationModule.forRoot({
      autoSchemaFile: true,
      transformSchema: (schema: GraphQLSchema) => {
        // console.log(schema)
        schema = applyMiddleware(schema, permissions);
        // console.log(schema)
        return schema;
      }
    }),
  ],
})
export class UsersModule {}
